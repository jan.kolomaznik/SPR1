package jm.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Controller
public class HelloController {

    // tag::doGet[]
    @GetMapping("/hello") // <1>
    public void doGet(
            HttpServletRequest request, // <2>
            HttpServletResponse response // <2>
    ) throws IOException {
        PrintWriter out = response.getWriter(); // <3>
        String username = request.getParameter("username");
        if (username != null && username.length() > 0) {
            out.println("Hello " + username + "!");
        } else {
            out.println("Hello world!");
        }
    }
    // end::doGet[]
}
