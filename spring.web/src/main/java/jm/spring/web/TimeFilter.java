package jm.spring.web;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

// @Component
public class TimeFilter implements Filter {

    // tag::doFilter[]
    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        response.getWriter().println("Start at: " + LocalDateTime.now().format(DateTimeFormatter.ISO_TIME));
        chain.doFilter(request, response);
        response.getWriter().println("End at: " + LocalDateTime.now().format(DateTimeFormatter.ISO_TIME));
    }
    //end::doFilter[]

}
