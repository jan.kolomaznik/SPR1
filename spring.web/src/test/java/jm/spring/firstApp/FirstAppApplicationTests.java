package jm.spring.firstApp;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;


// tag::SpringBootTest[]
@SpringBootTest //<1>
class FirstAppApplicationTests {

	@Autowired //<2>
	private CommandLineRunner commandLineRunner; //<3>

	@Test
	void contextLoads() {
		assertThat(commandLineRunner).isNotNull(); //<4>
	}
}
// tag::SpringBootTest[]
