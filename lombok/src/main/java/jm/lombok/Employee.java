package jm.lombok;

import lombok.Builder;

import java.time.LocalDate;

// tag::Builder[]
@Builder
public class Employee {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        Employee person = Employee.builder()
                .firstName("Pepa")
                .lastName("Zdepa")
                .build();
        System.out.println(person);
    }
}
// end::Builder[]
