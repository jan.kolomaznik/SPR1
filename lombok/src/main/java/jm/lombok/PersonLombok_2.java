package jm.lombok;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

// tag::Getter_Setter[]
@Getter @Setter
public class PersonLombok_2 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_2 person = new PersonLombok_2();
        person.setFirstName("Tomas");
        person.setLastName("Skveli");
        System.out.println("firstName: " + person.getFirstName());
        System.out.println("lastName: " + person.getLastName());
    }
}
// end::Getter_Setter[]

