package cz.ictpro.spr1.AddressBook.person

import spock.lang.Specification
import spock.lang.Unroll

import java.time.LocalDate

class PersonServiceTest extends Specification {

    private PersonService personService
    private PersonRepository personRepository

    void setup() {
        personService = new PersonService()
        personRepository = personService.personRepository = Mock(PersonRepository)
    }

    void cleanup() {
    }

    def "GetAllPersons"() {
    }

    def "GetPersonById"() {
    }

    def "Create Person: basic scenario."() {
        given:
        def person = new Person(name: "Pepa", gender: Gender.MALE, birthDay: LocalDate.now())

        when:
        def result = personService.createPerson("Jirka", person.birthDay, person.gender)

        then:
        1 * personRepository.save(_ as Person) >> { Person p  -> p }
        result.name == person.name
    }

    @Unroll
    def "Unification Name: #name --> #result."() {
        expect:
        personService.unificationName(name) == result

        where:
        name         | result
        "pepa"       | "Pepa"
        "JIRKA"      | "Jirka"
        "1234567890" | "12345678"
    }
}
