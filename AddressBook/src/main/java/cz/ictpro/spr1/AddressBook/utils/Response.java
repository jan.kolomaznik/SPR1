package cz.ictpro.spr1.AddressBook.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import org.springframework.context.expression.MapAccessor;
import org.springframework.hateoas.Link;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Getter
@JsonInclude(NON_NULL)
public class Response<E> {

    public static <E> Response<E> of(Optional<E> data) {
        return Response.of(data.orElseThrow(NotFoundException::new));
    }

    public static <E> Response<E> of(E data) {
        return new Response<>(data);
    }

    public Response() {}

    private Response(E data) {
        this.data = data;
    }

    private E data;

    private Map<String, String> links;

    public Response<E> addLink(Link link) {
        if (links == null) {
            links = new LinkedHashMap<>();
        }
        links.put(link.getRel().value(), link.getHref());
        return this;
    }


}
