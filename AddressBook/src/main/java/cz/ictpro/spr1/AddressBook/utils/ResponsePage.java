package cz.ictpro.spr1.AddressBook.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.Value;
import org.springframework.data.domain.Page;

import javax.validation.Valid;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Getter
@JsonInclude(NON_NULL)
public class ResponsePage<R> {

    @Getter
    public static class Page {

        public int pageSize;
        public int pageNumber;
        public int totalPages;
        public long totalElements;
        //public Sort sort;
    }

    public static <R> ResponsePage of(org.springframework.data.domain.Page<R> page) {
        ResponsePage<R> rp = new ResponsePage<>();
        rp.items = page.getContent();
        rp.page.pageNumber = page.getNumber();
        rp.page.pageSize = page.getSize();
        rp.page.totalElements = page.getTotalElements();
        rp.page.totalPages = page.getTotalPages();
        return rp;
    }

    public ResponsePage() {
    }

    private List<R> items;

    private Page page = new Page();


}
