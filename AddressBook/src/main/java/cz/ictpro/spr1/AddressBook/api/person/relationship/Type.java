package cz.ictpro.spr1.AddressBook.api.person.relationship;

public enum Type {

    FRIENDS, LOVER, ENEMY;
}
