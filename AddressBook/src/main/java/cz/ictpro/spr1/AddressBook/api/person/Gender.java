package cz.ictpro.spr1.AddressBook.api.person;

public enum Gender {

    MALE, FEMALE
}
