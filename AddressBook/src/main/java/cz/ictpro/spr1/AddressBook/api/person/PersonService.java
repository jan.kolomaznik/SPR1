package cz.ictpro.spr1.AddressBook.api.person;

import cz.ictpro.spr1.AddressBook.api.person.relationship.Relationship;
import cz.ictpro.spr1.AddressBook.api.person.relationship.RelationshipRepository;
import cz.ictpro.spr1.AddressBook.utils.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private RelationshipRepository relationshipRepository;

    /**
     * This method read collection of {@link Person} from database.
     * @return collection of persons.
     */
    Page<Person> getAllPersons(Pageable pageable) {
        log.debug("All person are read.");
        return personRepository.findAll(pageable);
    }

    /**
     * Find one {@link Person} from database.
     */
    Optional<Person> getPersonById(UUID id) {
        log.debug("Read person with id {}.", id);
        return personRepository.findById(id);
    }

    /**
     * Person factory methods to create new {@link Person} in system.
     */
    Person createPerson(String name, LocalDate birthDay, Gender gender) {
        log.info("Create person: name = {}, birthDay = {}, gender = {}.", name, birthDay, gender);
        Person person = new Person();
        person.setName(unificationName(name));
        person.setBirthDay(birthDay);
        person.setGender(gender);
        return personRepository.save(person);
    }

    private String unificationName(String name) {
        return StringUtils.capitalize(name.toLowerCase());
    }

    @Transactional
    public Page<Relationship> getPersonRelationships(UUID id, Pageable pageable) {
        return personRepository.findById(id)
                .map(p -> relationshipRepository.findAllByFrom(p, pageable))
                .orElseThrow(NotFoundException::new);
    }
}
