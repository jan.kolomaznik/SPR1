package cz.ictpro.spr1.AddressBook.api.person.address;

import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Value
@Builder
public class Address {

    private UUID id;
    private String street;
    private int number;
}
