package cz.ictpro.spr1.AddressBook.api.person;

import cz.ictpro.spr1.AddressBook.api.person.relationship.Relationship;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

// @NoRepositoryBean // JDBC Template
@Repository
public interface PersonRepository extends PagingAndSortingRepository<Person, UUID> {

    @Query("select p from Person as p where p.height > :vyska")
    Iterable<Person> najdiSVyskouvetsiNez(@Param("vyska") int vyska);
}
