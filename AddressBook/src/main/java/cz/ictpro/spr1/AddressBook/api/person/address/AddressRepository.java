package cz.ictpro.spr1.AddressBook.api.person.address;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public class AddressRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Address> findAll() {
        return jdbcTemplate.query(
                "SELECT * FROM ADDRESSES",
                new BuilderRowMapper<>(Address.builder())
        );
    }

    @Slf4j
    public static class BuilderRowMapper<T> implements RowMapper<T> {

        private Object builder;
        private List<Method> methods;
        private Class builderClass;

        public BuilderRowMapper(Object builder) {
            this.builder = builder;
            this.builderClass = builder.getClass();
            this.methods = Arrays.stream(builderClass.getDeclaredMethods())
                    .filter(m -> m.getParameterCount() == 1)
                    .peek(m -> log.info(m.toString()))
                    .collect(Collectors.toList());
        }

        @SneakyThrows
        @Override
        public T mapRow(ResultSet rs, int rowNum) throws SQLException {
            for (Method method : methods) {
                int index = rs.findColumn(method.getName());
                // todo test overeni, ze sloupec v rs je.
                Object[] param = new Object[1];
                switch (method.getParameterTypes()[0].toString()) {
                    case "class java.util.UUID":
                        param[0] = UUID.nameUUIDFromBytes(rs.getBytes(index));
                        break;
                    case "int":
                        param[0] = rs.getInt(index);
                        break;
                    case "class java.lang.String":
                        param[0] = rs.getString(index);
                        break;
                }
                method.invoke(builder, param);
            }
            Method build = builderClass.getDeclaredMethod("build", new Class[0]);
            return (T) build.invoke(builder);
        }
    }
}
