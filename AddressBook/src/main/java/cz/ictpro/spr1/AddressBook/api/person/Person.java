package cz.ictpro.spr1.AddressBook.api.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.ictpro.spr1.AddressBook.api.person.relationship.Relationship;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;

@Data
@Entity
@Table(name = "persons")
public class Person {

    @Created
    private LocalDateTime created;

    @Id
    //@JsonIgnore
    @EqualsAndHashCode.Exclude
    private UUID id = UUID.randomUUID();

    private String name;
    private LocalDate birthDay;

    @Enumerated(EnumType.STRING)
    @Column(length = 16)
    private Gender gender;
    private Integer height;

    @JsonIgnore
    @OneToMany(mappedBy = "to")
    private Collection<Relationship> relationshipTo;

    @JsonIgnore
    @OneToMany(mappedBy = "from")
    private Collection<Relationship> relationshipFrom;
}
