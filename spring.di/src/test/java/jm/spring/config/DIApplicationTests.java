package jm.spring.config;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DIApplicationTests {

	// tag::contextWithTwoCarDao[]
	@Autowired
	private CarDao carDao;

	@Autowired
	private CarDao vipCarDao;

	@Test
	void contextWithTwoCarDao() {
		assertThat(carDao).isNotNull(); // <1>
		assertThat(vipCarDao).isNotNull(); // <1>
		assertThat(carDao).isNotSameAs(vipCarDao); // <2>
	}
	// end::contextWithTwoCarDao[]

	// tag::propertyInjectService[]
	@Autowired
	private PropertyInjectService propertyInjectService;

	@Test
	void propertyInjectService() {
		assertThat(propertyInjectService.getCarDao()).isNotNull(); // <1>
	}
	// end::propertyInjectService[]

	//tag::constructorInjectService[]
	@Autowired
	private ConstructorInjectService constructorInjectService;

	@Test
	void constructorInjectService() {
		assertThat(constructorInjectService.getCarDao()).isNotNull(); // <1>
	}
	//end::constructorInjectService[]

	//tag::autowiredArray[]
	@Autowired
	private CarDao[] carDaoArray;

	@Test
	void autowiredArray() {
		assertThat(carDaoArray.length).isEqualTo(2); // <1>
	}
	//end::autowiredArray[]
}
