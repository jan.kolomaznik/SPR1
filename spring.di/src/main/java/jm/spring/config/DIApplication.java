package jm.spring.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.util.Arrays;

@SpringBootApplication
public class DIApplication {

	public static void main(String[] args) {
		SpringApplication.run(DIApplication.class, args);
	}

// tag::vipCarDao[]
	@Bean
	public CarDao vipCarDao() {
		return new CarDao();
	}
// end::vipCarDao[]

	@Bean
	public String nameA(CarDao vipCarDao) {
		return vipCarDao.toString();
	}

	@Bean
	public String nameB() {
		return vipCarDao().toString();
	}

	@Bean
	public String nameC() {
		return vipCarDao().toString();
	}

	@Bean
	public CommandLineRunner clr(ApplicationContext context) {
		return args -> {
			Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
		};

	}
}
