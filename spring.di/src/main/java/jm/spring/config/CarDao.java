package jm.spring.config;

import org.springframework.stereotype.Component;

// tag::CarDao[]
@Component
public class CarDao {
    // ... methods and atributes
}
// end::CarDao[]